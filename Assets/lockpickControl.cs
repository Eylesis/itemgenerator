﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lockpickControl : MonoBehaviour
{
    public float xMin, xMax, zMin;
    public float[] centerPos;
    private bool ready = false, ready2 = false;
    public float speed;
    public float difficulty;
    int tumblrSelected;

    public GameObject[] tumblrs;
    GameObject selectedTumblrHousing;
    GameObject selectedTumblr;
    System.Random rng = new System.Random();
    bool playing = false;
    // Use this for initialization
    void Start ()
    {
        randTumblrGoals();
    }

	void Update ()
    {
        if (ready)
        {
            ready2 = true;
            if (Input.GetMouseButton(0))
            {
                if (!playing)
                    GetComponent<AudioSource>().time = 19.0f;
                
                ready2 = false;
                int toLock = tumblrSelected - 1;

                if (toLock < 0)
                    toLock = 0;

                gameObject.transform.position = new Vector3(centerPos[toLock], gameObject.transform.position.y, gameObject.transform.position.z);
                print(transform.eulerAngles.z + " > " + (zMin + 360));

                selectedTumblrHousing = tumblrs[tumblrSelected - 1];
                selectedTumblr = selectedTumblrHousing.transform.GetChild(0).gameObject;

                if (transform.eulerAngles.z == 0 || transform.eulerAngles.z > (zMin + 360))
                {
                    print(transform.eulerAngles.z - 360);
                    gameObject.transform.Rotate(new Vector3(0, 0, -(gameObject.transform.rotation.z + Time.deltaTime * speed)));
                    if (selectedTumblrHousing.GetComponent<TumblrCollider>().getState() == false)
                    {
                        selectedTumblr.transform.position -= new Vector3(0, Time.deltaTime * speed / 10, 0);

                        if (selectedTumblr.transform.position.y - 3 >= (selectedTumblrHousing.GetComponent<TumblrCollider>().tumblrKey - (difficulty+0.02f))
                        && selectedTumblr.transform.position.y - 3 <= (selectedTumblrHousing.GetComponent<TumblrCollider>().tumblrKey + difficulty))
                        {

                            //selectedTumblr.transform.position += new Vector3(((float)rng.Next(-1, 2)) / 100f, 0, ((float)rng.Next(-1, 2)) / 100f;
                            if (!playing)
                                GetComponent<AudioSource>().Play();
                            playing = true;
                        }
                        else
                        {
                            GetComponent<AudioSource>().Stop();
                        }
                    }
                }

            }

            if (Input.GetMouseButtonUp(0))
            {
                GetComponent<AudioSource>().Stop();
                playing = false;
                ready2 = true;
                transform.rotation = Quaternion.identity;

                //print((selectedTumblrHousing.GetComponent<TumblrCollider>().tumblrKey - 0.02f) + " : " + selectedTumblr.transform.position.y + " : " + (selectedTumblrHousing.GetComponent<TumblrCollider>().tumblrKey + 0.02f));
                if (selectedTumblr.transform.position.y-3 >= (selectedTumblrHousing.GetComponent<TumblrCollider>().tumblrKey - difficulty)
                    && selectedTumblr.transform.position.y-3 <= (selectedTumblrHousing.GetComponent<TumblrCollider>().tumblrKey + difficulty))
                    selectedTumblrHousing.GetComponent<TumblrCollider>().setLocked(true);
                else
                    selectedTumblr.transform.position = selectedTumblrHousing.GetComponent<TumblrCollider>().origin;
            }

            if (ready2)
            {
                float mousePos = Input.GetAxis("Mouse X");
                //print(mousePos);
                if (gameObject.transform.position.x <= xMax && gameObject.transform.position.x >= xMin)
                {
                    gameObject.transform.position += new Vector3(mousePos * Time.deltaTime * speed, 0, 0);
                }

                if (gameObject.transform.position.x > xMax)
                    gameObject.transform.position = new Vector3(xMax, gameObject.transform.position.y, gameObject.transform.position.z);
                if (gameObject.transform.position.x < xMin)
                    gameObject.transform.position = new Vector3(xMin, gameObject.transform.position.y, gameObject.transform.position.z);
            }
        }  
    }
    public void setReady(bool _ready)
    {
        ready = _ready;
    }

    public void setSelectedTumblr(int tumblr)
    {
        tumblrSelected = tumblr;
    }

    public void randTumblrGoals()
    {
        foreach (GameObject tumblr in tumblrs)
            tumblr.GetComponent<TumblrCollider>().tumblrKey = ((float)rng.Next(50, 71)) / 100f;
    }
}
