﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.UI;

public class generator : MonoBehaviour
{
    public GameObject nameText;
    public GameObject typeText;
    public GameObject bodyText;
    public GameObject BG;

    public int commonmax;
    public int uncommonmax;
    public int seed;
    public class DataHold
    {
        public List<string> weapons;
        public List<string> prefixes;
        public List<string> suffixes;
        public List<string> properties;
    }
    DataHold randomTable;
    string[] weapon;
    System.Random rng;
    // Use this for initialization
    void Awake()
    {
        weapon = new string[5];
        randomTable = new DataHold();
        randomTable = LoadJson("randomTables.json");
        weapon = generateWeapon(randomTable);
        updateCard(weapon, rng.Next(1, 100));
    }
	
	// Update is called once per frame
	public void genWeapon()
    {
            weapon = generateWeapon(randomTable);

            int rarity = rng.Next(0, 3);
            print(string.Format("{1} {0} {2}", weapon));
            updateCard(weapon, rng.Next(1, 101));
	}

    DataHold LoadJson(string filename)
    {
        using (StreamReader r = new StreamReader("Assets/" + filename))
        {
            string json = r.ReadToEnd();
            DataHold randomTable = new DataHold();
            randomTable  = JsonUtility.FromJson<DataHold>(json);
            return randomTable;
        }
    }

    string[] generateWeapon(DataHold data)
    {
        rng = new System.Random();
        int[] genweapon = new int[3];
        genweapon[0] = rng.Next(0, data.weapons.Count);
        genweapon[1] = rng.Next(0, data.prefixes.Count);
        genweapon[2] = rng.Next(0, data.suffixes.Count);
        while (genweapon[1] == genweapon[2])
            genweapon[2] = rng.Next(0, data.suffixes.Count);

        string[] genweaponNames = new string[5] { data.weapons[genweapon[0]], data.prefixes[genweapon[1]], data.suffixes[genweapon[2]], data.properties[genweapon[1]], data.properties[genweapon[2]]};

        return genweaponNames;
        //print(data.prefixes[prefix] + " " + data.weapons[weapon] + " " + data.suffixes[suffix]);
    }

    void updateCard(string[] data, int rarity)
    {
        seed = rarity;
        if (rarity >= 1 && rarity <= commonmax)
        {
            BG.GetComponent<Image>().color = new Color(185f / 255, 185f / 255, 185f / 255);
            nameText.GetComponent<Text>().text = string.Format("{0}", data);
            typeText.GetComponent<Text>().text = string.Format("Weapon({0})", data);
            bodyText.GetComponent<Text>().text = string.Format("This is a mundane {0}.", data);
        }
        else if (rarity >= commonmax+1 && rarity <= uncommonmax)
        {
            BG.GetComponent<Image>().color = new Color(135f / 255, 150f / 255, 255f / 255);
            nameText.GetComponent<Text>().text = string.Format("{0} {2}", data);
            typeText.GetComponent<Text>().text = string.Format("Weapon({0}), uncommon", data);
            bodyText.GetComponent<Text>().text = string.Format("{3}", data);
        }
        else if(rarity >= uncommonmax+1 && rarity <= 100)
        {
            BG.GetComponent<Image>().color = new Color(200f / 255f, 150f / 255, 0);
            nameText.GetComponent<Text>().text = string.Format("{1} {0} {2}", data);
            typeText.GetComponent<Text>().text = string.Format("Weapon({0}), rare", data);
            bodyText.GetComponent<Text>().text = string.Format("{3}\n\n{4}", data);
        }
    }
}
