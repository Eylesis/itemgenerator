﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{
    public int Fade = 0;

    public GameObject[] fadeObjs;
    GameObject fadeObj;

    public void setFade(int _fade)
    {
        Fade = _fade;
    }

    void Update()
    {
        if (Fade == 1)
        {
            foreach (GameObject target in fadeObjs)
            {
                fadeObj = target;
                fadeout();
            }
            Fade = 0;
        }
        if (Fade == 2)
        {
            foreach(GameObject target in fadeObjs)
            {
                fadeObj = target;
                fadein();
            }
            Fade = 0;
        }
    }

    void fadeout()
    {
        print("fading.");
        SetMaterialTransparent();
        iTween.FadeTo(fadeObj, 0, 2);
        Fade = 0;
    }

    void fadein()
    {
        iTween.FadeTo(fadeObj, 1, 1);
        Invoke("SetMaterialOpaque", 0.9f);
    }

    private void SetMaterialTransparent()
    {
        foreach (Material m in fadeObj.GetComponent<Renderer>().materials)
        {
            m.SetFloat("_Mode", 2);

            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);

            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);

            m.SetInt("_ZWrite", 0);

            m.DisableKeyword("_ALPHATEST_ON");

            m.EnableKeyword("_ALPHABLEND_ON");

            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");

            m.renderQueue = 3000;

        }

    }

    private void SetMaterialOpaque()
    {
        foreach (GameObject fadeObj in fadeObjs)
        {
            foreach (Material m in fadeObj.GetComponent<Renderer>().materials)
            {
                print(m.name);

                m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);

                m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);

                m.SetInt("_ZWrite", 1);

                m.DisableKeyword("_ALPHATEST_ON");

                m.DisableKeyword("_ALPHABLEND_ON");

                m.DisableKeyword("_ALPHAPREMULTIPLY_ON");

                m.renderQueue = 2000;
            }
        }
    }
}