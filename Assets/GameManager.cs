﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //public GameObject[] cameraAngles;
    public GameObject lockpick;
    public GameObject chestLid;
    public GameObject[] tumblrs;
    public GameObject cameraAngleDefault;
    public GameObject cameraAngleZoom;
    public GameObject cameraAngleReward;
    public GameObject cameraFocusZoom;
    public GameObject lockpickReadied;
    public GameObject lockpickUnreadied;
    public GameObject chestFocus;
    public Transform[] zoomPath;
    public GameObject[] cards;
    public GameObject[] cardPts;

    bool check = false;
    public GameObject chest;
    

    Hashtable ht = new Hashtable();
    
    void Awake()
    {
        
        ht.Add("time", 2);
        ht.Add("looktarget", cameraFocusZoom.transform);
        ht.Add("path", zoomPath);
        ht.Add("easetype", "easeInSine");
        
    }
    void Start ()
    {
        
        gameObject.transform.position = cameraAngleDefault.transform.position;
        gameObject.transform.rotation = cameraAngleDefault.transform.rotation;

        foreach (GameObject card in cards)
        {
            card.SetActive(false);
        }
        }
	
	void Update ()
    {
        
        if (Input.GetKeyUp(KeyCode.Space) && check == false)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;
            ht["path"] = zoomPath;
            ht["looktarget"] = cameraFocusZoom.transform;
            iTween.MoveTo(this.gameObject, ht);
            iTween.MoveTo(lockpick, iTween.Hash("position", lockpickReadied.transform, "delay", 3, "time", 2));
            Invoke("unlockLockpick", 5f);
            chest.GetComponent<FadeOut>().setFade(1);
            check = true;
        }

        if(check)
        {
            if (checkTumblrs(tumblrs))
            {
                ht["position"] = cameraAngleDefault.transform;
                ht.Remove("looktarget");
                ht.Remove("path");
                iTween.MoveTo(this.gameObject, ht);
                iTween.LookTo(this.gameObject, iTween.Hash("looktarget", chestFocus.transform.position, "time", 2f, "delay", 1.5f));
                chest.GetComponent<FadeOut>().setFade(2);
                iTween.MoveTo(lockpick, iTween.Hash("position", lockpickUnreadied.transform, "time", 2));
                lockpick.GetComponent<lockpickControl>().setReady(false);
                check = false;
                resetTumblrs(tumblrs);
                lockpick.GetComponent<lockpickControl>().randTumblrGoals();
                iTween.RotateTo(chestLid, iTween.Hash("rotation", new Vector3(0,0,-80), "time", 2, "delay", 2));

                
                Invoke("activateCards", 0.5f);
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
        }

    }
    void activateCards()
    {
        int i = 0;
        foreach (GameObject card in cards)
        {
            card.SetActive(true);
            card.GetComponent<generator>().genWeapon();
            iTween.MoveTo(card, iTween.Hash("position", cardPts[i].transform.position, "time", 2f, "delay", 2.5f));
            iTween.RotateTo(card, iTween.Hash("rotation", new Vector3(0, 90, 0), "time", 2f, "delay", 2.5f));
            i++;
        }
    }
    void unlockLockpick()
    {
        lockpick.GetComponent<lockpickControl>().setReady(true);
    }
    bool checkTumblrs(GameObject[] _tumblrs)
    {
        foreach (GameObject proxyTumblr in _tumblrs)
        {
            if (proxyTumblr.GetComponent<TumblrCollider>().getState() == true)
            {
            }
            else
                return false;
        }
        return true;
    }
    void resetTumblrs(GameObject[] _tumblrs)
    {
        foreach(GameObject proxyTumblr in _tumblrs)
        {
            proxyTumblr.GetComponent<TumblrCollider>().setLocked(false);
            proxyTumblr.transform.GetChild(0).transform.position = proxyTumblr.GetComponent<TumblrCollider>().origin;
        }
    }
}
