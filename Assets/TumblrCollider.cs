﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TumblrCollider : MonoBehaviour {

    public GameObject pickObj;
    public int tumblrID = 0;
    public float tumblrKey;
    public Vector3 origin;
    public GameObject manager;
    bool locked = false;
	// Use this for initialization
	void Start () {


        
        origin = gameObject.transform.GetChild(0).transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        print(tumblrID + ": hit " + other.name);
        if (other.name == pickObj.name)
        {
            pickObj.GetComponent<lockpickControl>().setSelectedTumblr(tumblrID);
        }
    }
    public void setLocked(bool state)
    {
        locked = state;
    }
    public bool getState()
    {
        return locked;
    }
}
