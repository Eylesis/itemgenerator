﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class growshrink : MonoBehaviour
{

    public Vector3 originalSize;
    public Vector3 maxSize;

    void Awake()
    {
        originalSize = this.gameObject.transform.localScale;
    }
    void OnMouseEnter()
    {
        while (this.gameObject.transform.localScale.x < maxSize.x)
        {
            this.gameObject.transform.localScale += new Vector3(Time.deltaTime * 10, Time.deltaTime * 10, 0);
        }
        print("enter");
    }
    void OnMouseExit()
    {
        while (this.gameObject.transform.localScale.x > originalSize.x)
        {
            this.gameObject.transform.localScale -= new Vector3(Time.deltaTime * 10, Time.deltaTime * 10, 0);

            if (this.gameObject.transform.localScale.x < originalSize.x)
            {
                this.gameObject.transform.localScale = originalSize;
                break;
            }
        }
        print("exit");
    }
}